import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppService} from './app.service';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {AudioPlayerService} from './audio-player.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule
    ],
    providers: [
        AppService,
        AudioPlayerService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
