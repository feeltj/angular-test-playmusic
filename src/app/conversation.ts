export class Conversation {
    image: string;
    l1speaker: string;
    l1text: string;
    l2text: string;
    soundUrl: string;
    speaker: string;
    audioPlaying: boolean;
}
