import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppService} from './app.service';
import {Subscription} from 'rxjs/Subscription';
import {Conversation} from './conversation';
import {AudioPlayerService} from './audio-player.service';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/delay';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    description: string;
    conversations: Conversation[];
    isAutoPlaying = false;

    private initSub: Subscription;
    private _audio: HTMLAudioElement;
    private playSub: Subscription;
    private index = 0;

    constructor(private service: AppService,
                private audioPlayerService: AudioPlayerService) {
        this._audio = new Audio();
    }

    onPlaySoundClick(idx: number) {
        const line = this.conversations[idx];
        if (this.playSub && !this.playSub.closed) {
            this.playSub.unsubscribe();
        }
        this.playSub = this.playSound(line).subscribe();
    }

    autoPlay(idx: number) {
        const line = this.conversations[idx];
        if (this.playSub && !this.playSub.closed) {
            this.playSub.unsubscribe();
        }
        this.playSub = this.playSound(line)
            .delay(1000)
            .subscribe(() => {
                this.index++;
                this.autoPlay(this.index);
            });
    }

    onPlayAllClick() {
        this.isAutoPlaying = !this.isAutoPlaying;
        if (this.isAutoPlaying) {
            this.autoPlay(this.index);
        }
    }

    private playSound(line: Conversation) {
        return this.audioPlayerService.playSound(line.soundUrl)
            .map((evt) => {
                if (evt.type === 'canplay') {
                    line.audioPlaying = true;
                }
                return evt;
            })
            .last()
            .map(() => line.audioPlaying = false);
    }

    /**
     * @inheritDoc
     */
    ngOnInit(): void {
        this.initSub = this.service.init()
            .subscribe((data: { conversations: Conversation[], description: string }) => {
                this.description = data.description;
                this.conversations = data.conversations;
            });
    }

    /**
     * @inheritDoc
     */
    ngOnDestroy(): void {
        if (this.initSub && !this.initSub.closed) {
            this.initSub.unsubscribe();
        }
    }

}
