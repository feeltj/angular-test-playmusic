import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Conversation} from './conversation';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AppService {

    constructor(private http: Http) {
    }


    init(): Observable<{ conversations: Conversation[], description: string }> {
        return this.http.get('unit1/data/conversation/SPAco_ENGus_008_040_CP.xml')
            .map((res) => parseConversation(res.text()));
    }


}

function parseConversation(xml) {
    const parser = new DOMParser();
    const xmlData = parser.parseFromString(xml, 'application/xml');
    const conversations: Conversation[] = [];
    let description: string;
    if (xmlData.getElementsByTagName('description')[0]) {
        description = xmlData.getElementsByTagName('description')[0].innerHTML;
    }
    const lines = xmlData.getElementsByTagName('line');
    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];
        const conversation = new Conversation();
        conversation.l1text = line.getAttribute('l1text');
        conversation.l2text = line.getAttribute('l2text');
        conversation.speaker = line.getAttribute('speaker');
        conversation.l1speaker = line.getAttribute('l1speaker');
        if (line.hasAttribute('soundUrl')) {
            conversation.soundUrl = 'unit1/' + line.getAttribute('soundUrl');
        }
        const image = line.getElementsByTagName('image')[0];
        if (image) {
            conversation.image = `unit1/${image.textContent}`;
        }
        conversations.push(conversation);
    }
    return {conversations, description};
}
