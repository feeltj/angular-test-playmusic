import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';

@Injectable()
export class AudioPlayerService {
    private _audio: HTMLAudioElement;

    constructor() {
        this._audio = new Audio();
    }

    playSound(url: string): Observable<any> {
        return Observable.create((observer: Observer<any>) => {
            this._audio.onplaying = observer.next.bind(observer);
            this._audio.oncanplay = observer.next.bind(observer);
            this._audio.onratechange = observer.next.bind(observer);
            this._audio.ondurationchange = observer.next.bind(observer);
            this._audio.onerror = observer.error.bind(observer);
            this._audio.onended = observer.complete.bind(observer);
            this._audio.onpause = () => {
                this._audio.pause();
                this._audio.load();
                observer.complete();
            };
            this._audio.src = url;
            this._audio.play();
            return () => {
                this._audio.pause();
                this._audio.onended = null;
                this._audio.onerror = null;
                this._audio.ondurationchange = null;
                observer.complete();
            };
        });
    }

}
